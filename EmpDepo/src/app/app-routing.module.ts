import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TopNotchDressesComponent } from './top-notch-dresses/top-notch-dresses.component';
import { HandbagsComponent } from './handbags/handbags.component';
import { JewelleryComponent } from './jewellery/jewellery.component';

const routes: Routes = [
  {path:'',component:HomeComponent },
  {path:'home',component:HomeComponent },//canActivate:[AuthGuard]},
  {path:'aboutus',component:AboutUsComponent},
  {path:'contactus',component:ContactUsComponent},
  {path:'top-notch-dresses',component:TopNotchDressesComponent},
  {path: 'handBags', component : HandbagsComponent},
  {path: 'jewellery', component : JewelleryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
