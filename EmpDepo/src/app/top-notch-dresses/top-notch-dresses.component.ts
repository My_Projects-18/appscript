import { Component } from '@angular/core';

@Component({
  selector: 'app-top-notch-dresses',
  templateUrl: './top-notch-dresses.component.html',
  styleUrl: './top-notch-dresses.component.css'
})
export class TopNotchDressesComponent {
  topnotchdresses = [
    
      
    { 
      
      image: 'https://img.faballey.com/images/Product/TOP07033Z/2.jpg', 
      description: 'Green Floral Print V-Neck Peplum Blouse' ,
      
    },
    { 
      
      image: 'https://img.faballey.com/images/Product/TOP06916Z/2.jpg', 
      description: 'Light Pink Wrap Peplum Top',
      //price:'MRP = 600'
    },
    { 
       
      image: 'https://img.faballey.com/images/Product/TOP06756Z/2.jpg', 
      description: 'Purple Square Neck Shimmer Crop Top' ,
      //price:'MRP = 550'
    }
    // Add more coffee items as needed
  ];

  // Inject CartService
  //constructor(private cartService: CartItemsService) {}

  
}
