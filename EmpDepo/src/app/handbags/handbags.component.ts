import { Component } from '@angular/core';

@Component({
  selector: 'app-handbags',
  templateUrl: './handbags.component.html',
  styleUrl: './handbags.component.css'
})
export class HandbagsComponent {

  handBags = [
    
      
    { 
      
      image: 'https://th.bing.com/th?id=OPAC.TTOJNI0Q%2fZfszQ474C474&w=220&h=210&c=17&o=5&dpr=1.5&pid=21.1', 
      description: 'MYADDICTION Women PU Leather Shoulder Bag Tote Bag Handba' ,
      
    },
    { 
      
      image: 'https://th.bing.com/th?id=OPAC.UbuMId1lwCjs%2fQ474C474&w=220&h=210&c=17&o=5&dpr=1.5&pid=21.1', 
      description: 'Yelloe Women Brown Embellished Solid Tote Bag With Pouch',
      //price:'MRP = 600'
    },
    { 
       
      image: 'https://th.bing.com/th?id=OPAC.bbFzW2ljT9GWMg474C474&w=220&h=210&c=17&o=5&dpr=1.5&pid=21.1', 
      description: 'Purple Square Neck Shimmer Crop Top' ,
      //price:'MRP = 550'
    }
    // Add more coffee items as needed
  ];
}
