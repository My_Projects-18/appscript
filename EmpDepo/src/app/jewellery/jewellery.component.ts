import { Component } from '@angular/core';

@Component({
  selector: 'app-jewellery',
  templateUrl: './jewellery.component.html',
  styleUrl: './jewellery.component.css'
})
export class JewelleryComponent {

  jewelleries = [
    
      
    { 
      
      image: 'https://th.bing.com/th?id=OPAC.q4lOh2QGo95AZg474C474&w=160&h=213&c=17&dpr=1.5&pid=21.1', 
      description: 'Gold-Plated Off-White Stone-Studded & Beaded Temple Jewellery Set' ,
      
    },
    { 
      
      image: 'https://th.bing.com/th?id=OPAC.k3RU%2bd8TgMnFxg474C474&w=165&h=220&c=17&dpr=1.5&pid=21.1', 
      description: 'Gold-Plated White & Pink Pearl Beaded Jewellery Set',
      //price:'MRP = 600'
    },
    { 
       
      image: 'https://www.bing.com/th?id=OPAC.7bbunpd2bBIDdQ474C474&o=5&pid=21.1&w=160&h=213&rs=1&qlt=100&dpr=1.5', 
      description: 'Gold-Plated CZ Studded & Crystal Beaded Jewellery Set' ,
      //price:'MRP = 550'
    }
    // Add more coffee items as needed
  ];
}
