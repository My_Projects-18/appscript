import { Component } from '@angular/core';
// import { MessageDialogComponent } from '../message-dialog/message-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrl: './contact-us.component.css'
})
export class ContactUsComponent {
  formData = {
    name: '',
    email: '',
    message: ''
  };

  // constructor(private dialog: MatDialog) {}

  // onSubmit() {
    // Here you can handle form submission logic, such as sending the data to a server
    // For demonstration purposes, we'll just show a popup message and reset the form data
  //   this.openDialog();
  //   this.formData = { name: '', email: '', message: '' };
  // }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(MessageDialogComponent, {
  //     width: '250px',
  //     data: 'Your response has been recorded.'
//     });

//     dialogRef.afterClosed().subscribe(result => {
//       console.log('The dialog was closed');
//     });
//   }
}
