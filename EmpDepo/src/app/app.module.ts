import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { MatDialogModule } from '@angular/material/dialog';
import { TopNotchDressesComponent } from './top-notch-dresses/top-notch-dresses.component';
import { HandbagsComponent } from './handbags/handbags.component';
import { JewelleryComponent } from './jewellery/jewellery.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
     HeaderComponent,
    HomeComponent,
    AboutUsComponent,
    ContactUsComponent,
    TopNotchDressesComponent,
    HandbagsComponent,
    JewelleryComponent,
    FooterComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
